var observableArray = require("data/observable-array");
var observable = require("data/observable");
var frameModule = require("ui/frame");
var bluetooth = require("nativescript-bluetooth");
var dialogs = require("ui/dialogs");
var index;

function pageLoaded(args) {
    var page = args.object;

    // the Observable-wrapped objects from the previous page
    var peripheral = page.navigationContext.peripheral;
    var service = page.navigationContext.service;
    service.peripheral = peripheral;
    page.bindingContext = new observable.Observable(service);
    index = service.characteristics.findIndex((c) => { return c.UUID == '00035b03-58e6-07dd-021a-08123a000301'; })
    if (index > -1) {
        service.characteristics[index].last_data = new Uint8Array();
        onCharacteristicTap({ index: index, object: page });
    }
}

function clearBuffer(args) {
    var page = args.object;
    var service = page.bindingContext;
    var characteristic = service.characteristics[index];
    console.log(characteristic.last_data.toString(), characteristic.last_data.length);
    characteristic.last_data = new Uint8Array();
}

function onCharacteristicTap(args) {
    var index = args.index;
    var page = args.object;
    var service = page.bindingContext;
    var characteristic = service.characteristics[index];

    // show an actionsheet which contains the most relevant possible options
    var p = characteristic.properties;
    var actions = [];

    function getTimestamp() {
        return new Date().toLocaleString();
    }

    function mergeUint8Arrays(a, b) {
        a = a || new Uint8Array();
        b = b || new Uint8Array();
        var c = new Uint8Array(a.length + (b || []).length);
        c.set(a);
        c.set(b, a.length);

        return c;
    }

    function getInt16(msb, lsb) {
        c = new DataView(new ArrayBuffer(2));
        c.setInt16(0, (msb << 8) + lsb);
        return c.getInt16(0);
    }

    if (!(characteristic.notify)) {
        bluetooth.startNotifying({
            peripheralUUID: service.peripheral.UUID,
            serviceUUID: service.UUID,
            characteristicUUID: characteristic.UUID,
            onNotify: function(result) {
                // result.value is an ArrayBuffer. Every service has a different encoding.
                // fi. a heartrate monitor value can be retrieved by:
                // var recvStr = String.fromCharCode.apply(null, data);
                // var heartRate = data[1];
                var data = new Uint8Array(result.value);
                characteristic.last_data = mergeUint8Arrays(characteristic.last_data, data);
                if (characteristic.last_data.length >= 8) {
                    console.log(JSON.stringify(result));
                    [
                        { name: 'adcc', data_msb: characteristic.last_data[0], data_lsb: characteristic.last_data[1] },
                        { name: 'gyroX', data_msb: characteristic.last_data[2], data_lsb: characteristic.last_data[3] },
                        { name: 'gyroY', data_msb: characteristic.last_data[4], data_lsb: characteristic.last_data[5] },
                        { name: 'gyroZ', data_msb: characteristic.last_data[6], data_lsb: characteristic.last_data[7] },
                    ].forEach(function(i) {
                        console.log(JSON.stringify(i));
                        service.set(i.name, getInt16(i.data_msb, i.data_lsb));
                    })
                    characteristic.last_data = characteristic.last_data.slice(8);
                }
                service.set("feedback", result.value);
                service.set("feedbackRaw", result.valueRaw);
                service.set("feedbackTimestamp", getTimestamp());
            }
        }).then(function(result) {
            characteristic.set("notify", true);
            service.set("feedback", 'subscribed for notifications');
            service.set("feedbackTimestamp", getTimestamp());
        });
    } else {
        bluetooth.stopNotifying({
            peripheralUUID: service.peripheral.UUID,
            serviceUUID: service.UUID,
            characteristicUUID: characteristic.UUID
        }).then(function(result) {
            characteristic.set("notify", false);
            service.set("feedback", 'notification stopped');
            service.set("feedbackTimestamp", getTimestamp());
        }, function(error) {
            service.set("feedback", error);
        });
    }
}

exports.pageLoaded = pageLoaded;
exports.clearBuffer = clearBuffer;
exports.onCharacteristicTap = onCharacteristicTap;