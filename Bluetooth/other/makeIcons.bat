@echo off
IF %1.==. GOTO No1
REM iOS files
convert "%1" -resize 29x29!     "../app/App_Resources/iOS/icon-Small.png"
convert "%1" -resize 40x40!     "../app/App_Resources/iOS/icon-Small-40.png"
convert "%1" -resize 50x50!     "../app/App_Resources/iOS/icon-Small-50.png"
convert "%1" -resize 57x57!     "../app/App_Resources/iOS/icon.png"
convert "%1" -resize 58x58!     "../app/App_Resources/iOS/icon-Small@2x.png"
convert "%1" -resize 60x60!     "../app/App_Resources/iOS/icon-60.png"
convert "%1" -resize 72x72!     "../app/App_Resources/iOS/icon-72.png"
convert "%1" -resize 76x76!     "../app/App_Resources/iOS/icon-76.png"
convert "%1" -resize 80x80!     "../app/App_Resources/iOS/icon-Small-40@2x.png"
convert "%1" -resize 100x100!   "../app/App_Resources/iOS/icon-Small-50@2x.png"
convert "%1" -resize 114x114!   "../app/App_Resources/iOS/icon@2x.png"
convert "%1" -resize 120x120!   "../app/App_Resources/iOS/icon-60@2x.png"
convert "%1" -resize 144x144!   "../app/App_Resources/iOS/icon-72@2x.png"
convert "%1" -resize 152x152!   "../app/App_Resources/iOS/icon-76@2x.png"
convert "%1" -resize 180x180!   "../app/App_Resources/iOS/icon-60@3x.png"
  
REM Android files
convert "%1" -resize 36x36!    "../app/App_Resources/Android/drawable-ldpi/icon.png"
convert "%1" -resize 48x48!    "../app/App_Resources/Android/drawable-mdpi/icon.png"
convert "%1" -resize 72x72!    "../app/App_Resources/Android/drawable-hdpi/icon.png"
REM convert "%1" -resize 96x96!    "../app/App_Resources/Android/drawable-xhdpi/icon.png"
REM convert "%1" -resize 144x144!  "../app/App_Resources/Android/drawable-xxhdpi/icon.png"
REM convert "%1" -resize 192x192!  "../app/App_Resources/Android/drawable-xxxhdpi/icon.png"



GOTO End

:No1
  ECHO No param 1
GOTO End

:End